import React from 'react'
import { createStore, combineReducers } from 'redux'
import { Provider } from 'react-redux'
import Navigation from './screens/Navigation'

// import Reducers
import userReducers from './store/userReducers'

const store = createStore(
  combineReducers({
    userReducers
  }), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

const App = () => {
  return (
    <Provider store={store}>
      <Navigation />
    </Provider>
  )
}

export default App
