import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import FlexContainer from '../../components/FlexContainer'

const styles = StyleSheet.create({})

const C = (props) => {
  return (
    <FlexContainer title='C' onPress={() => props.navigation.navigate('D')} />

  )
}

export default C
