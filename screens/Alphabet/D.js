import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import FlexContainer from '../../components/FlexContainer'

const styles = StyleSheet.create({})

const D = (props) => {
  return (
    <FlexContainer title='D' onPress={() => props.navigation.popToTop()} />

  )
}

export default D
