import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import FlexContainer from '../../components/FlexContainer'

const styles = StyleSheet.create({

})

const A = (props) => {
  return (
    <FlexContainer title='A' onPress={() => props.navigation.navigate('B')} />

  )
}

export default A
