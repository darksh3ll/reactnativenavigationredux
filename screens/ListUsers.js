import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
const ListUsers = () => {
  return (
    <View style={styles.container}>
      <Text>Articles</Text>
    </View>
  )
}

ListUsers.navigationOptions = {
  title: 'Articles',
  headerStyle: {
    backgroundColor: '#7259c1'
  },
  headerTitleStyle: {
    fontWeight: 'bold',
    color: 'white'
  }
}

export default ListUsers
