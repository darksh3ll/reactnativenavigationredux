import React,{useState} from 'react'
import {View,Text,StyleSheet,Button} from "react-native";
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import {useDispatch, useSelector} from "react-redux";
import ListUsers from "./ListUsers";

const styles = StyleSheet.create({
    multislider: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        borderBottomWidth: 0.3,
        backgroundColor: 'white',
        height: 60,
    },
});

const SearchUsers = (props) => {
    const dispatch = useDispatch();
    const users = useSelector(state => state.userReducers.data);
    const [minAge,setminAge] = useState(16);
    const [maxAge,setmaxAge] = useState(99);

    const _ageChanged = (values) => {
        setminAge(values[0]);
        setmaxAge(values[1])
    };

    const searchAge = () => {
        dispatch({ type: 'ORDER',payload:{minAge,maxAge} })
        props.navigation.goBack()
    };

    return (
        <View>
            <View style={styles.multislider}>
                <Text>{minAge}</Text>
                <MultiSlider
                    sliderLength={190}
                    values={[minAge, maxAge]}
                    min={minAge}
                    max={maxAge}
                    onValuesChange={_ageChanged}
                />
                <Text>{maxAge}</Text>
            </View>
            <Button
                title="Lancer la recherche"
             onPress={searchAge}/>
        </View>

    )
};
SearchUsers.navigationOptions = props => ({
    title: 'SearchUsers',

});



export default SearchUsers