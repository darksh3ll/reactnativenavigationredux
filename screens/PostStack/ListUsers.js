import React, { useEffect } from 'react'
import { StyleSheet, TouchableOpacity } from 'react-native'
import { FlatList } from 'react-navigation'
import { Icon } from 'react-native-elements'
import axios from 'axios'
import { useDispatch, useSelector } from 'react-redux'
import ListPerformance from '../../components/ListPerformance'
const styles = StyleSheet.create({

});

// composants

const RenderItem = ({ item, navigation }) => (
  <TouchableOpacity onPress={() => navigation.push('CardUser', { item })}>
    <ListPerformance item={item} />
  </TouchableOpacity>
)

const SearchButton = ({ onPress }) => (
  <TouchableOpacity onPress={onPress} style={{ padding: 10 }}>
    <Icon
      size={30}
      name='search'
      color='white'
    />
  </TouchableOpacity>
);

// Function

const ListUsers = (props) => {
  const dispatch = useDispatch();
  const users = useSelector(state => state.userReducers.data);

  const shearUser = () => {
      props.navigation.navigate("SearchUsers")
  };

  useEffect(() => {
    props.navigation.setParams({ shearUser: shearUser })
    axios
      .get('https://randomuser.me/api/?results=100')
      .then(result => dispatch({ type: 'FETCH_DATA', payload: result.data.results }))
      .catch(error => { console.log(error) })
  }, [dispatch]);

  return (
    <FlatList
      refreshing
      data={[
        ...users
      ]}
      keyExtractor={(item, index) => index.toString()}
      initialNumToRender={10}
      removeClippedSubviews
      maxToRenderPerBatch={10}
      onEndReachedThreshold={0.5}
      renderItem={({ item }) => (
        <RenderItem item={item} navigation={props.navigation} />
      )}
    />
  )
};

ListUsers.navigationOptions = props => ({
  title: 'Articles',
  headerRight: <SearchButton onPress={props.navigation.getParam('shearUser')} />
});

export default ListUsers
