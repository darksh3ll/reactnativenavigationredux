import React from 'react'
import { StyleSheet, View, ActivityIndicator } from 'react-native'

import { Image, Text } from 'react-native-elements'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
const CardUser = (props) => {
  const item = props.navigation.state.params.item
  const { phone, email } = item
  return (
    <View style={styles.container}>
      <Image
        source={{ uri: item.picture.large }}
        style={{ width: 150, height: 150 }}
        PlaceholderContent={<ActivityIndicator />}
      />
      <Text>{phone}</Text>
      <Text>{email}</Text>
    </View>
  )
}

export default CardUser
