import { createAppContainer, createSwitchNavigator } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { createBottomTabNavigator } from 'react-navigation-tabs'

import Home from '../screens/Home'
import A from '../screens/Alphabet/A'
import B from '../screens/Alphabet/B'
import C from '../screens/Alphabet/C'
import D from '../screens/Alphabet/D'
import One from '../screens/Numbers/One'
import Two from '../screens/Numbers/Two'
import Three from './Numbers/Three'
import UserList from '../screens/PostStack/ListUsers'
import CardUser from '../screens/PostStack/CardUser'
import SearchUsers from '../screens/PostStack/SearchUsers'


const defaultNavigationOptions = {
  headerStyle: { backgroundColor: '#451770' },
  headerTintColor: 'white',
  headerBackTitle: null,
  titleStyle: { color: 'white' }
}

// Vue
const HomeStack = createStackNavigator({
  Home: Home
})

const AlphbetStack = createStackNavigator(
  {
    A: A,
    B: B,
    C: C,
    D: D
  },
  {
    defaultNavigationOptions
  }

)

const NumberStack = createStackNavigator(
  {
    One: One,
    Two: Two,
    Three: Three
  },
  {
    defaultNavigationOptions
  }
)

const PostStack = createStackNavigator(
  {
    UserList: UserList,
    CardUser: CardUser,
      SearchUsers:SearchUsers
  },
  {
    defaultNavigationOptions
  }
)

// App Principal
const AppStack = createBottomTabNavigator(
  {
    Alphabet: AlphbetStack,
    Numbers: NumberStack,
    Post: PostStack
  },

  {
    order: ['Numbers', 'Alphabet', 'Post'],
    tabBarOptions: {
      activeBackgroundColor: '#451770',
      inactiveBackgroundColor: '#FFF',
      inactiveTintColor: 'black',
      activeTintColor: 'white',
      labelStyle: '#FFF',
      showLabel: true
    }

  }
)

const SwitchNavigator = createSwitchNavigator(
  {
    HomeStack: HomeStack,
    AppStack: AppStack
  },
  { initialRouteName: 'HomeStack' }
)

export default createAppContainer(SwitchNavigator)
