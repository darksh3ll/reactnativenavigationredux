import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { StyleSheet, Text, View, Button } from 'react-native'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#202020'
  },
  text: {
    color: '#36fff9',
    fontSize: 20

  }
})

const Home = ({ navigation }) => {
  // REDUX
  const dispatch = useDispatch()
  const user = useSelector(state => state.userReducers.data)

  return (
    <View style={styles.container}>
      <Text style={styles.text}>React navigation 4.0.10</Text>
      <Button
        title='Entrez'
        onPress={() => navigation.navigate('One')}
      />
    </View>
  )
}

Home.navigationOptions = {
  title: 'Home',
  headerStyle: {
    backgroundColor: '#7259c1'
  },
  headerTitleStyle: {
    fontWeight: 'bold',
    color: 'white'
  }
}

export default Home
