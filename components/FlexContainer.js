import React from 'react';
import {StyleSheet,TouchableOpacity,Text,View } from 'react-native';

const styles = StyleSheet.create({
    container : {
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor: "#131131",
    },
    text:{
        color:"#ffffff",
        fontSize:40

    }
});

const  FlexContainer = ({onPress,title}) => {
    return (
        <View style={styles.container}>
        <TouchableOpacity onPress={onPress} >
           <Text style={styles.text}>{title}</Text>
        </TouchableOpacity>
        </View>

    );
};

export default FlexContainer
