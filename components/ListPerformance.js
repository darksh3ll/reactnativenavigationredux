import React from 'react';
import { StyleSheet } from 'react-native';
import { ListItem } from "react-native-elements";

const styles = StyleSheet.create({
    container: {
        borderBottomWidth:0.3,
    },
});

const ListPerformance = React.memo((props) => {
    const {item} = props;
    const fullName = `${item.name.first} ${item.name.last}`;
    return (
        <ListItem
            leftAvatar={{source: {uri: item.picture.large}}}
            title={fullName}
            subtitle={item.location.city}
            bottomDivider
            chevron
        />

    );
})

export default ListPerformance
