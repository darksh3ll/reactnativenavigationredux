import React from 'react';
import {StyleSheet, TextInput} from 'react-native';

const styles = StyleSheet.create({
    textInput: {
        borderWidth: 0.3,
        minWidth: 300,
        height: 40
    }
});
const InputForm = ({placeholder, onChangeText,value}) => {
    return (
            <TextInput
                style={styles.textInput}
                placeholder={placeholder}
                onChangeText={onChangeText}
                value={value}
            />
    );
};

export default InputForm;
