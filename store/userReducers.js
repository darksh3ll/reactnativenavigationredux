const initialState = {
  data: ''
};

export default (state = initialState, { type, payload }) => {
  console.log(state.data);
  switch (type) {
    case 'FETCH_DATA':
      return { ...state, data: payload }
    case 'ORDER':
      return { ...state, data: state.data.filter(user => user.dob.age >= payload.minAge && user.dob.age <= payload.maxAge)};
    default:
      return state
  }
}
